<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
    <title>Read More, Walk More</title>
    <script type="text/javascript" src="https://cdn.bootcss.com/jquery/2.1.4/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/css/select2.min.css" />
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/js/select2.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.singleSelect').select2(); 
            $('.multiSelect').select2();
        });
    </script>
</head>

<body style="background-color:#E8F9F4;">
    <nav class="navbar navbar-dark bg-dark mb-4">
        <div class="container-fluid">
            <a class="navbar-brand align-items-center fw-bold" href="#">
                <img src="logo1.png" alt="" width="30" height="30" class="d-inline-block align-top">
                Read More, Walk More
            </a>
        </div>
    </nav>
    <div class="card mx-auto" style="width: 40rem;">
        <div class="card-header bg-dark text-white fw-bold fs-4">
            註冊
        </div>
        <div class="card-body">
            <form class="" action="signup3.php" method="post">
                <div class="row">
                    <div class="col-6 mb-3">
                        <label for="phone" class="form-label">電話：</label>
                        <input type="tel" class="form-control" id="phone" name="phone">
                    </div>
                    <div class="col-6">
                        <p>任教學校：</p>
                        <?php
                        require_once("dbtools.inc.php");
                        $county = $_COOKIE["county"];
                        $grade = $_COOKIE["grade"];
                        $link = create_connection() or die("connecting fails!");
                        $count = 0;
                        $arr = [];
                        if ($grade == "elementary_school") {
                            $sql = "SELECT `school_Name`, `county` FROM elementary_school";
                            $result = execute_sql($link, "U0733105", $sql);
                            while ($row = mysqli_fetch_assoc($result)) {
                                if ($row['county'] == $county) {
                                    $count++;
                                    $arr[] = $row['school_Name'];
                                }
                            }
                        } elseif ($grade == "junior_high_school") {
                            $sql = "SELECT `school_Name`, `county` FROM junior_high_school";
                            $result = execute_sql($link, "U0733105", $sql);
                            while ($row = mysqli_fetch_assoc($result)) {
                                if ($row['county'] == $county) {
                                    $count++;
                                    $arr[] = $row['school_Name'];
                                }
                            }
                        } else {
                            $sql = "SELECT `school_Name`, `county` FROM senior_high_school";
                            $result = execute_sql($link, "U0733105", $sql);
                            while ($row = mysqli_fetch_assoc($result)) {
                                if ($row['county'] == $county) {
                                    $count++;
                                    $arr[] = $row['school_Name'];
                                }
                            }
                        }
                        echo '<select class="singleSelect" aria-label="Default select example" name="school">';
                        foreach ($arr as $value) {
                            echo '<option value="' . $value . '">' . $value . '</option>';
                        }
                        echo '</select>';
                        ?>
                    </div>
                </div>
                <div class="mb-3">
                    <p>任教科目：</p>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="social" name="tch[]">
                        <label class="form-check-label" for="inlineCheckbox2">社會</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" id="inlineCheckbox3" value="geography" name="tch[]">
                        <label class="form-check-label" for="inlineCheckbox3">地理</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" id="inlineCheckbox4" value="history" name="tch[]">
                        <label class="form-check-label" for="inlineCheckbox4">歷史</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" id="inlineCheckbox4" value="citizen" name="tch[]">
                        <label class="form-check-label" for="inlineCheckbox4">公民</label>
                    </div>
                </div>
                <div class="d-grid col-12 d-md-flex justify-content-md-end">
                    <input type="submit" class="btn btn-primary col-2" value="註冊">
                </div>
            </form>
        </div>
    </div>
</body>

</html>